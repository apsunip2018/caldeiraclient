package sockets.client;

import java.io.InputStream;
import java.util.Scanner;

class NoticeSender implements Runnable {

    private InputStream server;

    public NoticeSender(InputStream server) {
        this.server = server;
    }

    public void run() {
        try(Scanner s = new Scanner(this.server)){
            while (s.hasNextLine()) {
                System.out.print("Nova memsage de etvim");
                System.out.println(s.nextLine());
            }
        }
    }
}