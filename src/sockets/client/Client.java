package sockets.client;

import client.frontend.ClientFrame;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Client extends Thread{

    private String host;
    private int port;

    public Client(String host, int port, settingsPage sPage) {
        this.host = host;
        this.port = port;
    }


    @Override
    public void run(){
        try {
            Socket client = new Socket(this.host, this.port); 
            Scanner key = new Scanner(System.in); 
            PrintStream saida = new PrintStream(client.getOutputStream());

            System.out.println("A caldeira foi conectada e está sendo monitorada pelo servidor!");

            NoticeSender r = new NoticeSender(client.getInputStream());
            new Thread(r).start();
            
            ClientFrame teste = new ClientFrame();
            
            teste.setVisible(true);
            
            while(true){
                saida.println(teste.getValueSlider().toString());
                sleep(1000);
            }
//            while (key.hasNextLine()) {
//                System.out.println(teste.getValueSlider().toString());
//                saida.println(key.nextLine());
//            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(
                new JFrame(), 
                e.getMessage(), 
                "Warning!", 
                JOptionPane.WARNING_MESSAGE
            );
        }
    }
}
